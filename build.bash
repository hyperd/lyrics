#!/bin/bash

# build it's a simple golang cross-compiler that generates alpine linux compatible binaries
build () {

  rm -rf "$(pwd)"/releases/*

  docker run --rm -it -v "$PWD":/usr/src/app -w /usr/src/app golang:latest bash -c '
  for GOOS in darwin linux; do
      for GOARCH in 386 amd64; do
        export GOOS GOARCH
        CGO_ENABLED=0 GO111MODULE=on go build -ldflags="-w -s -X main.minversion=`date -u +.%Y%m%d.%H%M%S`" \
        -a -installsuffix "static" -o bin/lyrics-$GOOS-$GOARCH ./main.go
      done
  done
  '
}

build
package cmd

/*
Copyright © 2020 hyperd

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

import (
	"fmt"
	"strings"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/hyperd/services/lyrics"
)

// getCmd represents the get command
var getCmd = &cobra.Command{
	Use:   "get",
	Short: "Get song lyrics, by its author and title",
	Long: `Get song lyrics, by its author and title:

	lyrics get --artist "pearl jam" --title "alive"`,

	Run: func(cmd *cobra.Command, args []string) {

		artist := strings.ToTitle(viper.GetString("artist"))
		title := strings.ToTitle(viper.GetString("title"))

		results := lyrics.Service(artist, title)

		fmt.Printf("\n\033[1;32mArtist:\033[0m \033[36m%s\033[0m\n\033[1;32mTitle:\033[0m \033[36m%s\033[0m\n\n%s\n", artist, title, results)
	},
}

var artist string
var title string

func init() {
	getCmd.Flags().StringVarP(&artist, "artist", "a", "", "The song's artist")
	getCmd.MarkFlagRequired("artist")

	viper.BindPFlag("artist", getCmd.Flags().Lookup("artist"))

	getCmd.Flags().StringVarP(&title, "title", "t", "", "The song's title")
	getCmd.MarkFlagRequired("title")

	viper.BindPFlag("title", getCmd.Flags().Lookup("title"))

	rootCmd.AddCommand(getCmd)
}

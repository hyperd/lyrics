package lyrics

/*
Copyright © 2020 hyperd

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"os/signal"
	"syscall"
)

// Lyrics represents the api response object
type Lyrics struct {
	// Lyrics content
	Lyrics string
}

// Lyrics returns the results from the lyrics.ovh api
func Lyrics(artist string, title string) string {

	errs := make(chan error)
	go func() {
		c := make(chan os.Signal)
		signal.Notify(c, syscall.SIGINT, syscall.SIGTERM)
		errs <- fmt.Errorf("%s", <-c)
	}()

	var lyrics Lyrics
	go func() {

		searchURL := fmt.Sprintf("https://api.lyrics.ovh/v1/%s/%s.", artist, title)
		fmt.Println(searchURL)
		client := &http.Client{}

		req, _ := http.NewRequest("GET", searchURL, nil)

		response, err := client.Do(req)

		if err != nil {
			fmt.Println("Things have their own way of going awry, are you connected?")
			return
		}

		defer response.Body.Close()
		responseBody, _ := ioutil.ReadAll(response.Body)
		if response.Status == "200 OK" {

			errs <- json.Unmarshal([]byte(responseBody), &lyrics)

		}
	}()

	return lyrics.Lyrics
}

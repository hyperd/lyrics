# Lyrics

[![wercker status](https://app.wercker.com/status/500bbc14c674aa89df4d81437798e4b0/s/master "wercker status")](https://app.wercker.com/project/byKey/500bbc14c674aa89df4d81437798e4b0)

Lyrics is a CLI interface (**cobra** app) written in **golang**, to dig into **api.lyrics.ovh**

## What do I get

Songs lyrics in your terminal, no ads, no trackers, no bullshit.

## Install it

Get it:

```bash
go get gitlab.com/hyperd/lyrics
```

Or install a pre-compiled version, based on your os and architecture:

```bash
curl -o /usr/local/bin https://gitlab.com/hyperd/lyrics/-/raw/master/bin/lyrics-{your_os_architecture}   # check the available architectures
chmod +x /usr/local/bin
```

## Build it yourself

### Targetted build, based on your system/architecture

```bash
# change according to your system/architecture
CGO_ENABLED=0 GOARCH=[amd64|386] GOOS=[linux|darwin] go build -ldflags="-w -s" -a -installsuffix 'static' -o lyrics /main.go
```

#### Cross-platform build, leveraging the [build.bash](./build.bash) script

```bash
chmod +x build.bash && ./build.bash
```

Currently, the builds in the [bin](./bin/) folder are available for the following **platforms and architectures**:

- darwin-amd64;
- darwin-386;
- linux-amd64;
- linux-386.

## Usage

```bash
lyrics get --artist <artist_name> --title <title>
# e.g.:
lyrics get --artist "Andy Stott" --title "Violence"
```
